<%@ page import="main.java.UserResponse" %>
<div class="login-container">
    <div>

        <%
            if (session.getAttribute("userResponse") != null) {
                UserResponse aUser = (UserResponse) session.getAttribute("userResponse");
                if (aUser.getReason() != "") {
                    out.println("<div class='error'>" + aUser.getReason() + "</div>");
                }
            }
        %>

        <form action="${pageContext.request.contextPath}/" method="post">
            <label for="j_username">Username</label><br>
            <input type="text" id="j_username" name="j_username"
                   value=<%= session.getAttribute("j_username") != null ? (String)session.getAttribute("j_username") : "user"  %>><br><br>
            <label for="j_password">Password</label><br>
            <input type="j_password" id="j_password" name="j_password"
                   value="<%= session.getAttribute("j_password") != null ? (String)session.getAttribute("j_password") : "pass"  %>"><br><br>
            <input type="submit" name="signin" value="Log In">
        </form>

    </div>

</div>