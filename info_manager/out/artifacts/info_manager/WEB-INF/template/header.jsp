<%@ page import="main.java.UserResponse" %>
<div>
    <a href="${pageContext.request.contextPath}">
        <img src="${pageContext.request.contextPath}/images/lock.svg"/>
    </a>

    <%
        if (session != null && session.getAttribute("userResponse") != null) {
            int userID = ((UserResponse) session.getAttribute("userResponse")).getUser_id();
            if (userID > 0) out.println("<br><a href='"+request.getContextPath()+"/logout' class='logout'>Log Out</a>");
        }
    %>

</div>