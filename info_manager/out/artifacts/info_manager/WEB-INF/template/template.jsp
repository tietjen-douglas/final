<%--
  Concept came from this tutorial:
  http://wiki4.caucho.com/Java_EE_Servlet_tutorial_:_Using_JSPs_to_create_header,_footer_area,_formatting,_and_basic_CSS_for_bookstore
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
<head>
    <title>${param.title}</title>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/css/main.css" />
    <script src="${pageContext.request.contextPath}/js/main.js"></script>
</head>
<body>
<header>
    <div>
<jsp:include page="/WEB-INF/template/header.jsp"/>
    </div>
</header>

<main>
    <div>
<h1>${param.title}</h1>

<jsp:include page="/WEB-INF/content/${param.content}.jsp"/>
    </div>
</main>

<footer>
<jsp:include page="/WEB-INF/template/footer.jsp"/>
</footer>

</body>
</html>
