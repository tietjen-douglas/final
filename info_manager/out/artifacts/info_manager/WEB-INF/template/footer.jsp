<%@ page import="java.time.LocalDate" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page import="java.time.format.FormatStyle" %>
<div>Developed by Douglas Tietjen - BYU-Idaho CIT 360 Class<br>
Today is
    <%
        LocalDate today = LocalDate.now();
        String formattedDate = today.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG));
        out.print( formattedDate );
    %>
</div>
