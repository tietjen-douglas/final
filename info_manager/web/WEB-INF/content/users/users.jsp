<%--<%@ page import="main.java.dao.UserDAO" %>--%>
<%@ page import="main.java.User" %>
<%@ page import="java.util.List" %>

<%
    if (request.getParameter("del") != null) {
        User delUser = (User) request.getAttribute("delUser");
        if (delUser == null) {
            // User was not found to delete
            out.print("<div class=\"error\"><span class=\"closebtn\" onclick=\"this.parentElement.style.display='none';\">&times;</span>No user account found to delete!</div>");
        } else {
            // Confirm Deletion
            out.print("<form action=\"" + request.getContextPath() + "/manage/users/\" method=\"post\" name=\"frmDelete\">");
            out.print("<div class=\"error\"><a href=\"" + request.getContextPath() + "/manage/users/\" class=\"closebtn\">&times;</a>");
            out.print("Really delete user?<br><br>");
            out.print("<input type=\"hidden\" name=\"delID\" value=\"" + delUser.getUser_id() + "\">");
            out.print("<input type=\"submit\" name=\"btnDelete\" value=\"Delete " + delUser.getFull_name() + "\">");
            out.print("</div></form>");
            return;
        }
    }

    if (request.getParameter("deleted") != null) {
        out.print("<div class=\"success\"><span class=\"closebtn\" onclick=\"this.parentElement.style.display='none';\">&times;</span>");
        out.print("Deleted User Successfully");
        out.print("</div>");
    }

    if (request.getParameter("errorMsg") != null) {
        out.print("<div class=\"error\"><span class=\"closebtn\" onclick=\"this.parentElement.style.display='none';\">&times;</span>");
        out.print(request.getParameter("errorMsg"));
        out.print("</div>");
    }
%>

<div class="alLeft">
    <a href="${pageContext.request.contextPath}/manage/users/?add">Add User</a>
    <table>
        <tr>
            <th></th>
            <th>ID</th>
            <th>Name</th>
            <th>Username</th>
            <th></th>
        </tr>

        <%
            List<User> u = (List<User>) request.getAttribute("userList");
            if (u != null) {
                if (u.size() == 0) {
                    out.println("<tr><td colspan=\"5\" class=\"alCenter\">No Users Found</td></tr>");
                }
                for (User i : u) {
                    out.println("<tr><td><a href=\"" + request.getContextPath() + "/manage/users/?edit=" + i.getUser_id() + "\">Edit</a></td><td>"
                            + i.getUser_id() + "</td><td>"
                            + i.getFull_name() + "</td><td>"
                            + i.getUsername()
                            + "</td><td><a href=\"" + request.getContextPath() + "/manage/users/?del=" + i.getUser_id() + "\">Delete</a></td></tr>");
                }
            }

        %>
    </table>

</div>