<%@ page import="main.java.User" %>

<%
    // Initialize Form Variables
    User editUser = (User) request.getAttribute("editUser");
    String fFullName = request.getAttribute("fFullName") != null ? (String) request.getAttribute("fFullName") : "";
    String fUsername = request.getAttribute("fUsername") != null ? (String) request.getAttribute("fUsername") : "";
    String fPassword = request.getAttribute("fPassword") != null ? (String) request.getAttribute("fPassword") : "";
    String errorMsg = request.getAttribute("errorMsg") != null ? (String) request.getAttribute("errorMsg") : "";
    boolean isNewUser = Boolean.TRUE == request.getAttribute("isNewUser");
%>

<div class="alLeft">
    <%
        // If Error found, display here
        if (!errorMsg.isEmpty()) {
            out.print("<div class=\"error\"><span class=\"closebtn\" onclick=\"this.parentElement.style.display='none';\">&times;</span><ul>");
            out.print(errorMsg);
            out.print("</ul></div>");
        }

        if (isNewUser)
            out.print("New User");
        else
            out.print("Edit " + editUser.getFull_name());

    %>
    <hr>
    <form action="" method="post" name="frmUpdate">
        <label for="full_name">Full Name</label><br>
        <input type="text" name="full_name" id="full_name" value="<%=fFullName%>"><br>
        <label for="username">Username</label><br>
        <input type="text" name="username" id="username" value="<%=fUsername%>"><br>
        <label for="password">Password</label><br>
        <input type="password" name="password" id="password" value="<%=fPassword%>"><br>
        <br>
        <input type="submit" name="btnUpdate" value="
    <%
        if (isNewUser)
            out.print("Add User");
        else
            out.print("Update User");

    %>
    ">
    </form>

</div>
