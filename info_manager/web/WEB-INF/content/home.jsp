<%@ page import="main.java.UserResponse" %>
<%@ page import="main.java.Information" %>
<%@ page import="java.util.List" %>

<strong>
    <%
        out.println("Welcome " + ((UserResponse) session.getAttribute("userResponse")).getFull_name());
    %>
</strong>
<br><br>
<a href="${pageContext.request.contextPath}/manage/users/">Manage Users</a>

<br>
<form action="${pageContext.request.contextPath}/info/" method="post" name="frmSearch">
    <br>
    <input type="text" name="keywords" placeholder="Search Information">
    <input type="submit" name="btnSearch" value="Search">
</form>

<div class="alLeft">
    <a href="${pageContext.request.contextPath}/manage/users/?add">Add New Information</a>
    <table>
        <tr>
            <th></th>
            <th>Title</th>
            <th></th>
        </tr>
<%
    if (request.getAttribute("infoList") != null) {

        List<Information> inf = (List<Information>) request.getAttribute("infoList");
        if (inf != null) {
            if (inf.size() == 0) {
                out.println("<tr><td colspan=\"3\" class=\"alCenter\">No Information Found</td></tr>");
            }
            for (Information i : inf) {
                out.println("<tr><td><a href=\"" + request.getContextPath() + "/manage/users/?edit=" + i.getInformation_id() + "\">Edit</a></td>"
                        + "<td onclick=\"toggleInfo("+ i.getInformation_id() +");\">"
                        + i.getSearch_info()
                        + "<div id=\"info_" + i.getInformation_id() + "\" class=\"hidden\">" + i.getEncrypted_info() + "</div>"
                        + "</td><td>"
                        + "<a href=\"" + request.getContextPath() + "/manage/users/?del=" + i.getInformation_id() + "\">Delete</a></td></tr>");
            }
        }
    }
%>

    </table>

</div>
<br><br>

