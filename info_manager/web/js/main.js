function toggleInfo(infoId) {
    let div = document.getElementById("info_" + infoId);
    if (div.style.display == "block") {
        div.style.display = "none";
    } else {
        div.style.display = "block";
    }
}
