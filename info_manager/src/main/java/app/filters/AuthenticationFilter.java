package main.java.app.filters;

import main.java.UserResponse;
import main.java.http.HTTP;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * This Java filter demonstrates how to intercept the request
 * and transform the response to implement authentication feature.
 * for the website's back-end.
 *
 * @author www.codejava.net
 * https://www.codejava.net/java-ee/servlet/how-to-implement-authentication-filter-for-java-web-application
 * Modified by Douglas Tietjen
 */
@WebFilter("/*")
public class AuthenticationFilter implements Filter {

    private static final String[] loginExcludedURLs = {
            "/css", "/js", "/images"
    };

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpSession session = httpRequest.getSession(false);

        // Logging Off
        boolean logOff = httpRequest.getRequestURI().endsWith("logout");
        if (logOff) {
            if (session != null)
                session.removeAttribute("userResponse");
            forwardToLogin(request, response);
            return;
        }

        // UserResponse holds the status code and error messages as well as the user object
        UserResponse aUser = null;

        // If the session already contains a userResponse, then use that user
        if (session != null && session.getAttribute("userResponse") != null)
            aUser = (UserResponse) session.getAttribute("userResponse");

        // If coming from the login page and submitting the username and password
        if (request.getParameter("j_username") != null && request.getParameter("j_password") != null) {
            String user = request.getParameter("j_username");
            String pass = request.getParameter("j_password");

            // Check for User by credentials
            HTTP http = new HTTP();
            try {
                aUser = http.Authenticate(user, pass, false);
            } catch (Exception e) {
                // Reject Authentication if error
                System.err.println("Authentication Error: " + e.getMessage());
            }

            // Store the UserResponse Object during session
            if (session != null)
                session.setAttribute("userResponse", aUser);

            // Track values if they need to re-enter then it will remember what they typed before
            if (aUser == null || (aUser != null && aUser.getUser_id() <= 0)) {
                if (session != null) {
                    session.setAttribute("j_username", user);
                    session.setAttribute("j_password", pass);
                }
            }
        }

        // Set boolean if logged in or not.
        boolean isLoggedIn = (session != null && aUser != null && aUser.getStatusCode() == 200 && aUser.getUser_id() > 0);

        String loginURI = httpRequest.getContextPath() + "/login/login.jsp";

        // Compare the URL to our login URL, if it matches then it is a login request
        boolean isLoginRequest = httpRequest.getRequestURI().equals(loginURI);

        boolean isLoginPage = httpRequest.getRequestURI().endsWith("login.jsp");

        if (isLoggedIn && (isLoginRequest || isLoginPage)) {
            // the user is already logged in and user is trying to login again
            // just forward to search page
            RequestDispatcher dispatcher = request.getRequestDispatcher("/");
            dispatcher.forward(request, response);

        } else if (isLoggedIn || isLoginRequest || ExcludeURL(httpRequest)) {
            // logged in, continue the filter chain
            // allow the request to reach the destination
            chain.doFilter(request, response);

        } else {
            // the user is not logged in, so authentication is required
            // forward to the Login page
            forwardToLogin(request, response);
        }

    }

    private void forwardToLogin(ServletRequest request, ServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/login/login.jsp");
        dispatcher.forward(request, response);
    }

    // This is to exclude Images, CSS, javascript files, etc. that don't need authentication to access
    private boolean ExcludeURL(HttpServletRequest httpRequest) {
        String requestURL = httpRequest.getRequestURL().toString();

        for (String loginExcludedURL : loginExcludedURLs) {
            if (requestURL.contains(loginExcludedURL)) {
                return true;
            }
        }

        return false;
    }

    public AuthenticationFilter() {
    }

    public void destroy() {
    }

    public void init(FilterConfig fConfig) throws ServletException {
    }

}
