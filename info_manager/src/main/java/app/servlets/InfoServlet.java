package main.java.app.servlets;

import main.java.Information;
import main.java.http.HTTP;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "InfoServlet")
public class InfoServlet extends HttpServlet {
    // Initialize Form Variables
    private int editID;
    boolean isNewInfo;
    Information editInfo;
    private String fSearchInfo;
    private String fEncryptedInfo;
    private String errorMsg;

    protected void initializeVars(HttpServletRequest request) {
        editID = -1;
        fSearchInfo = (request.getParameter("searchInfo") != null) ? request.getParameter("searchInfo") : "";
        fEncryptedInfo = (request.getParameter("eInfo") != null) ? request.getParameter("eInfo") : "";
        errorMsg = "";
        editInfo = null;
        isNewInfo = true;
        editInfo = null;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // This is just for debugging purposes
        //outputRequestVars(request, "doPost");

        initializeVars(request);

        HTTP http = new HTTP();
        List<Information> infos = null;

        // Searching for info
        if (request.getParameter("keywords") != null) {
            try {
                infos = http.getInfoList(request.getParameter("keywords"));
                request.setAttribute("infoList", infos);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("/");
        dispatcher.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/");
        dispatcher.forward(request, response);
    }

}
