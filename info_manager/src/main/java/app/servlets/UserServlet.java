package main.java.app.servlets;

import main.java.User;
import main.java.UserResponse;
import main.java.http.HTTP;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.List;

@WebServlet(name = "UserUpdateServlet")
public class UserServlet extends HttpServlet {

    // Initialize Form Variables
    private int editID;
    boolean isNewUser;
    User editUser;
    private String fFullName;
    private String fUsername;
    private String fPassword;
    private String errorMsg;

    protected void initializeVars(HttpServletRequest request) {
        editID = -1;
        fFullName = (request.getParameter("full_name") != null) ? request.getParameter("full_name") : "";
        fUsername = (request.getParameter("username") != null) ? request.getParameter("username") : "";
        fPassword = (request.getParameter("password") != null) ? request.getParameter("password") : "";
        errorMsg = "";
        editUser = null;
        isNewUser = true;
        editUser = null;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // This is just for debugging purposes
        //outputRequestVars(request, "doPost");

        String url = request.getContextPath() + "/manage/users/";
        initializeVars(request);

        HTTP http = new HTTP();
        UserResponse userResp = null;

        // Submitting Form (Add or Edit User)
        if (request.getParameter("btnUpdate") != null) {
            try {
                setupVars(request, http);
                userResp = http.saveUser(editID, fFullName, fUsername, fPassword);
                if (userResp.getReason() != null) errorMsg = userResp.getReason();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            forwardToUserEdit(request, response);
            return;
        }

        //Confirmed Delete
        if (request.getParameter("btnDelete") != null) {
            if (request.getParameter("delID") != null) {
                int delID = Integer.parseInt(request.getParameter("delID"));
                try {
                    userResp = http.deleteUser(delID);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (!userResp.getReason().isBlank())
                    url += "?errorMsg=" + URLEncoder.encode(userResp.getReason(), StandardCharsets.UTF_8.toString());
                else
                    url += "?deleted=" + request.getParameter("delID");
            }
        }

        // Default redirection to user list if nothing else modifies the URL
        response.sendRedirect(url);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {

        initializeVars(request);

        HTTP http = new HTTP();

        // This is for debugging purposes
        //outputRequestVars(request, "doGet");

        // Adding or Editing a User
        if (request.getParameter("add") != null || request.getParameter("edit") != null) {
            try {
                setupVars(request, http);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            forwardToUserEdit(request, response);
        } else if (request.getParameter("del") != null) {
            // Delete the User
            int delID = -1;
            try {
                delID = Integer.parseInt(request.getParameter("del"));
            } catch (NumberFormatException ex) {
// Do nothing, leave as -1
            }
            try {
                User delUser = http.getUser(delID);
                request.setAttribute("delUser", delUser);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            RequestDispatcher dispatcher = request.getRequestDispatcher("/users/");
            dispatcher.forward(request, response);
        } else {
            // Get the list of users in the database

            List<User> users = null;
            try {
                users = http.getUsers();
                request.setAttribute("userList", users);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher("/users/");
            dispatcher.forward(request, response);
        }
    }

    protected void setupVars(ServletRequest request, HTTP http) throws IOException, InterruptedException {
        errorMsg = "";

        if (request.getParameter("add") != null || request.getParameter("edit") != null) {
            try {
                editID = Integer.parseInt(request.getParameter("edit"));
            } catch (NumberFormatException ex) {
                // Do nothing, we must be adding
                editID = -1;
            }
            if (editID > 0)
                editUser = http.getUser(editID);

            isNewUser = editUser == null;

            request.setAttribute("isNewUser", isNewUser);

            request.setAttribute("editUser", editUser);
            if (request.getParameter("btnUpdate") != null) {
                request.setAttribute("fFullName", request.getParameter("full_name"));
                request.setAttribute("fUsername", request.getParameter("username"));
                request.setAttribute("fPassword", request.getParameter("password"));
            } else {
                request.setAttribute("fFullName", editUser != null ? editUser.getFull_name() : "");
                request.setAttribute("fUsername", editUser != null ? editUser.getUsername() : "");
                request.setAttribute("fPassword", "");
            }
        }
    }

    public static void outputRequestVars(HttpServletRequest request, String location) {
        System.out.println("\n" + location + " Request Parameters\n-----------------------------");
        Enumeration<String> params = request.getParameterNames();
        while (params.hasMoreElements()) {
            String paramName = params.nextElement();
            System.out.println(paramName + ":" + request.getParameter(paramName));
        }
    }

    private void forwardToUserEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = "";
        // If have not done a post or an error exists, take to edit page.
        if (!errorMsg.isBlank() || request.getParameter("btnUpdate") == null) {
            url += "/users/edit/?id=" + editID;
        }

        // Update Variables before sending to page
        request.setAttribute("errorMsg", errorMsg);

        if (url.isBlank())
            // Sending back to user list
            response.sendRedirect(request.getContextPath() + "/manage/users/");
        else {
            // Visit the edit user page
            RequestDispatcher dispatcher = request.getRequestDispatcher(url);
            dispatcher.forward(request, response);
        }
    }

    private void recordError(Exception e) {
        // Unique usernames required
        if (e.getCause().toString().contains("username_UNIQUE") ||
                e.getCause().toString().contains("ConstraintViolationException"))
            errorMsg += "<li>Username already taken, choose another.</li>";
        else
            errorMsg += "<li>" + e.getCause() + "</li>";
    }
}
