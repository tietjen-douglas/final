package main.java.http;

import java.io.*;
import java.net.*;
import java.net.http.*;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.*;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;
import main.java.*;
import main.java.utils.Encryption;

public class HTTP {

    enum APICall {AUTHENTICATE, USER_SINGLE, USER_LIST, INFO_LIST}

    enum APICallType {GET, POST}

    // This would be changed to a live HTTPS site for production
    private final static String API_URL = "http://localhost:8081/api/";

    private static HttpClient client;
    private static Map<Object, Object> data = new HashMap<>();
    private static ObjectMapper objectMapper = new ObjectMapper();

    public HTTP() {
        client = HttpClient.newBuilder()
                .connectTimeout(Duration.ofSeconds(3))
                .followRedirects(HttpClient.Redirect.NORMAL)
                .build();
    }

    public static List<User> getUsers() throws IOException, InterruptedException {
        List<User> users = new ArrayList();
        // Form variables to be sent
        data.put("userList", true);

        users = apiCall(users, data, APICall.USER_LIST, false);

        return users;
    }

    public static User getUser(int userID) throws IOException, InterruptedException {
        User user = new User();
        // Form variables to be sent
        data.put("user", userID);

        user = apiCall(user, data, APICall.USER_SINGLE, false);

        return user;
    }

    public static UserResponse saveUser(int userID, String full_name, String username, String password) throws IOException, InterruptedException {
        UserResponse userResp = new UserResponse(200, "");

        // Form variables to be sent
        data.put("saveUser", userID);
        data.put("fullName", full_name);
        data.put("username", username);
        data.put("password", password);

        userResp = apiCall(userResp, data, APICall.USER_SINGLE, true);

        return userResp;
    }

    public static UserResponse deleteUser(int userID) throws IOException, InterruptedException {
        UserResponse userResp = new UserResponse(200, "");

        // Form variables to be sent
        data.put("deleteUser", userID);

        userResp = apiCall(userResp, data, APICall.USER_SINGLE, true);

        return userResp;
    }

    public static List<Information> getInfoList(String keywords) throws IOException, InterruptedException {
        List<Information> infos = new ArrayList();
        // Form variables to be sent
        data.put("list", keywords);

        infos = apiCall(infos, data, APICall.INFO_LIST, false);

        return infos;
    }

    public static <T> T apiCall(T element, Map<Object, Object> data, APICall apiCall, Boolean showHeaders) throws IOException, InterruptedException {
        String url = API_URL;
        UserResponse ur = null;
        InfoResponse ir = null;
        APICallType callType = APICallType.POST;

        try {
            // Prepare the URL
            switch (apiCall) {
                case AUTHENTICATE:
                    url += "authenticate";
                    ur = (UserResponse) element;
                    break;
                case USER_SINGLE:
                case USER_LIST:
                    url += "users";
                    break;
                case INFO_LIST:
                    url += "info/";
                    callType = APICallType.GET;
                    break;
            }

            boolean isValidLink = false;
            if (apiCall == APICall.INFO_LIST)
                isValidLink = ValidateLink(url, ir);
            else isValidLink = ValidateLink(url, ur);

            if (isValidLink) {

                // "https://httpbin.org/post" for testing posts
                HttpRequest request = null;
                if (callType == APICallType.GET) {
                    request = HttpRequest.newBuilder()
                            .uri(URI.create(AddParams(url, data)))
                            .GET()
                            .build();
                } else {
                    request = HttpRequest.newBuilder()
                            .uri(URI.create(url))
                            .header("Content-Type", "application/x-www-form-urlencoded")
                            .POST(ofFormData(data))
                            .build();
                }
                HttpResponse<String> response = client.send(request,
                        HttpResponse.BodyHandlers.ofString());


                if (apiCall == APICall.AUTHENTICATE) {
                    setResponseData((UserResponse) element, response);
                }

                //print response headers
                if (showHeaders) {
                    HttpHeaders headers = response.headers();
                    headers.map().forEach((k, v) -> System.out.println(k + ":" + v));
                }

                // Set properties for Jackson JSON to allow missing properties to be ignored
                //objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                //objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);


                // Convert JSON data to objects (element is a generic type)
                if (element instanceof List<?>) {
                    if (apiCall == APICall.USER_LIST)
                        element = (T) objectMapper.readValue(response.body(), new TypeReference<List<User>>() {
                        });
                    else if (apiCall == APICall.INFO_LIST) {
                        element = (T) objectMapper.readValue(response.body(), new TypeReference<List<Information>>() {
                        });
                    }
                } else {
                    if (apiCall != APICall.AUTHENTICATE || ((UserResponse) element).getStatusCode() == 200) {
                        //((apiCall == APICall.AUTHENTICATE && ((UserResponse) element).getStatusCode() == 200) || (apiCall != APICall.AUTHENTICATE))
                        element = objectMapper.readValue(response.body(), (Class<T>) element.getClass());
                    }
                    if (element instanceof UserResponse) {
                        setResponseData((UserResponse) element, response);
                    }
                }
            }
        } finally {
            data.clear();
        }
        return element;
    }

    public static String AddParams(String url, Map<Object, Object> data) {
        var builder = new StringBuilder();
        for (Map.Entry<Object, Object> entry : data.entrySet()) {
            if (builder.length() > 0) {
                builder.append("&");
            }
            builder.append(URLEncoder.encode(entry.getKey().toString(), StandardCharsets.UTF_8));
            builder.append("=");
            builder.append(URLEncoder.encode(entry.getValue().toString(), StandardCharsets.UTF_8));
        }
        return url + "?" + builder.toString();
    }
    /**
     * The Authenticate Method
     * Purpose: Calls the API_URL and uses Jackson JSON to create an object from the returned JSON
     */
    public static UserResponse Authenticate(String j_username, String j_password, boolean showHeaders) throws IOException, InterruptedException {
        UserResponse userResp = new UserResponse(201, "Unauthorized");

        // Encrypt the password with the date/time for authentication
        Encryption encryption = new Encryption();
        String ePassword = "";
        try {
            ePassword = encryption.encryptString(Globals.EncryptionKey, Instant.now().toString() + "_" + j_password);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        // Form variables to be sent
        data.put("j_username", j_username);
        data.put("j_password", ePassword);

        userResp = apiCall(userResp, data, APICall.AUTHENTICATE, false);

        return userResp;
    }

    // Code by: Ralph's Blog
    // https://golb.hplar.ch/2019/01/java-11-http-client.html
    // This allows me to convert my data to Form Data
    public static HttpRequest.BodyPublisher ofFormData(Map<Object, Object> data) {
        var builder = new StringBuilder();
        for (Map.Entry<Object, Object> entry : data.entrySet()) {
            if (builder.length() > 0) {
                builder.append("&");
            }
            builder.append(URLEncoder.encode(entry.getKey().toString(), StandardCharsets.UTF_8));
            builder.append("=");
            builder.append(URLEncoder.encode(entry.getValue().toString(), StandardCharsets.UTF_8));
        }
        return HttpRequest.BodyPublishers.ofString(builder.toString());
    }

    private static void setResponseData(UserResponse aUser, HttpResponse<String> response) {
        //Get the headers, status code and user object if returned.
        Map<String, List<String>> headers = response.headers().map();
        int statusCode = response.statusCode();
        String reason = (headers.get("reason") != null) ? headers.get("reason").toString() : "";
        // Strip [] that surrounds header values
        reason = reason.replaceAll("^\\[|]$", "");

        aUser.setStatusCode(statusCode);
        aUser.setReason(reason);
    }

    /**
     * The encodeValue Method
     * Purpose: Makes sure it is a URL friendly string
     *
     * @param value String - value to convert to URL friendly string
     * @return String
     */
    private static String encodeValue(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }
    }


    /**
     * The ValidateLink Method
     * Purpose: Testing a HTTP request quickly if it is valid or not.
     *
     * @param url      String - url to check for validity
     * @param userResp UserResponse - the Object to track the user and the status codes
     * @return Boolean - Was it valid or not
     */
    public static boolean ValidateLink(String url, UserResponse userResp) {
        try {
            HttpRequest request = HttpRequest.newBuilder().uri(URI.create(url)).GET().build();

            // Void response and don't need body as just checking for the response status code.
            HttpResponse<Void> response = client.send(request, HttpResponse.BodyHandlers.discarding());

            // Get status code and if in range of 200 to 299 then it is good.
            int statusCode = response.statusCode();
            if (userResp != null) userResp.setStatusCode(statusCode);
            boolean success = (statusCode >= 200 && statusCode <= 299);
            System.out.format("Status Code: %s - %s\n", statusCode, url);

            if (!success && userResp != null)
                userResp.setReason("Error " + statusCode + " - Unable to connect to server.");

            return success;

        } catch (IOException | InterruptedException e) {
            if (userResp != null) userResp.setReason("Error: " + e.toString());
            System.err.format("Exception: %s - %s\n", url, e.toString());
            return false;

        } catch (IllegalArgumentException e) {
            if (userResp != null) userResp.setReason("Invalid URL: " + e.getMessage());
            System.err.format("Invalid URL: %s - %s\n", url, e.getMessage());
            return false;
        }
    }

    public static boolean ValidateLink(String url, InfoResponse infoResp) {
        try {
            HttpRequest request = HttpRequest.newBuilder().uri(URI.create(url)).GET().build();

            // Void response and don't need body as just checking for the response status code.
            HttpResponse<Void> response = client.send(request, HttpResponse.BodyHandlers.discarding());

            // Get status code and if in range of 200 to 299 then it is good.
            int statusCode = response.statusCode();
            if (infoResp != null) infoResp.setStatusCode(statusCode);
            boolean success = (statusCode >= 200 && statusCode <= 299);
            System.out.format("Status Code: %s - %s\n", statusCode, url);

            if (!success && infoResp != null)
                infoResp.setReason("Error " + statusCode + " - Unable to connect to server.");

            return success;

        } catch (IOException | InterruptedException e) {
            if (infoResp != null) infoResp.setReason("Error: " + e.toString());
            System.err.format("Exception: %s - %s\n", url, e.toString());
            return false;

        } catch (IllegalArgumentException e) {
            if (infoResp != null) infoResp.setReason("Invalid URL: " + e.getMessage());
            System.err.format("Invalid URL: %s - %s\n", url, e.getMessage());
            return false;
        }
    }

}

