package main.java;

public class Information {
    private int information_id;
    private String search_info;
    private String encrypted_info;
    private int version;

    public Information() {
    }

    public Information(String search_info, String encrypted_info, int version) {
        this.search_info = search_info;
        this.encrypted_info = encrypted_info;
        this.version = version;
    }

    public String getSearch_info() {
        return search_info;
    }

    public void setSearch_info(String search_info) {
        this.search_info = search_info;
    }

    public int getInformation_id() {
        return information_id;
    }

    public void setInformation_id(int information_id) {
        this.information_id = information_id;
    }

    public String getEncrypted_info() {
        return encrypted_info;
    }

    public void setEncrypted_info(String encrypted_info) {
        this.encrypted_info = encrypted_info;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
