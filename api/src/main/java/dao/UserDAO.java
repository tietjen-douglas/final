package main.java.dao;

import main.java.hibernate.HibernateUtility;
import main.java.hibernate.User;
import main.java.utils.Encryption;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class UserDAO {

    /**
     * Original getUsers() and getUser() functions written by Troy Tuckett
     * Modified and added other functions by Douglas Tietjen
     **/
    public List<User> getUsers() {
        Session session = null;
        try {
            session = HibernateUtility.getSessionFactory().getSession();
            session.getTransaction().begin();
            String sql = "from main.java.hibernate.User";
            List<User> users = (List<User>) session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return users;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    /**
     * Used to get a single customer from database
     * Original written by Troy Tucket
     **/
    public User getUser(int user_id) {
        Session session = null;
        try {
            session = HibernateUtility.getSessionFactory().getSession();
            session.getTransaction().begin();
            String sql = "from main.java.hibernate.User where user_id=" + Integer.toString(user_id);
            User c = (User) session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return c;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public User getUserByCredentials(String username, String password) {
        Session session = null;

        try {
            session = HibernateUtility.getSessionFactory().getSession();

            session.getTransaction().begin();
            String sql = "from main.java.hibernate.User where username=:username";
            User u = (User) session.createQuery(sql)
                    .setParameter("username", username)
                    .getSingleResult();
            session.getTransaction().commit();

            System.out.println("User: " + u.getUser_id());

            // We found an account, let's check the password against the salted hashed password
            if (u != null) {
                String salt = u.getSalt();
                String hPass = Encryption.hashPassword(u.getUser_id(), salt, password);

                // Do they match?
                if (hPass.equals(u.getPassword())) {
                    System.out.println("User Validated: " + u.getUser_id());
                    return u;
                }
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    public void updateUser(User user) {
        Session session = null;
        try {
            session = HibernateUtility.getSessionFactory().getSession();
            session.beginTransaction();
            session.saveOrUpdate(user);
            session.flush();
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            session.close();
        }
    }

    public User addUser(User user) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtility.getSessionFactory().getSession();
            System.out.println("session : " + session);
            transaction = session.beginTransaction();
            session.save(user);
            // We need to add the record to get the ID so we can has the password with the ID in it.
            user.setPassword(Encryption.hashPassword(user.getUser_id(), user.getSalt(), user.getPassword()));
            session.saveOrUpdate(user);
            transaction.commit();
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            session.close();
        }
    }

    public void deleteUser(int id) {
        Session session = null;
        try {
            session = HibernateUtility.getSessionFactory().getSession();
            Transaction beginTransaction = session.beginTransaction();
            Query createQuery = session.createQuery("delete from main.java.hibernate.User u where u.user_id =:id");
            createQuery.setParameter("id", id);
            createQuery.executeUpdate();
            beginTransaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
    }


}
