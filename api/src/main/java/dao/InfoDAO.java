package main.java.dao;

import main.java.hibernate.HibernateUtility;
import main.java.hibernate.Information;
import org.hibernate.LockMode;
import org.hibernate.LockOptions;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ConcurrentModificationException;
import java.util.List;

public class InfoDAO {
    public Information getInfo(int info_id) {
        Session session = null;
        try {
            session = HibernateUtility.getSessionFactory().getSession();
            session.getTransaction().begin();
            Information c = null;
            c = (Information) session.createNamedQuery(Information.QUERY_BY_ID).setParameter("id", info_id).getSingleResult();
            session.getTransaction().commit();
            return c;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public Information addInfo(Information info) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtility.getSessionFactory().getSession();
            System.out.println("session : " + session);
            transaction = session.beginTransaction();
            session.save(info);
            transaction.commit();
            return info;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            session.close();
        }
    }

    public synchronized void updateInfo(Information info) {
        Session session = null;
        try {
            // Get current record in DB to compare version against
            Information iValidation = getInfo(info.getInformation_id());

            session = HibernateUtility.getSessionFactory().getSession();
            session.beginTransaction();

            if (info.getVersion() != iValidation.getVersion())
                throw new ConcurrentModificationException("Concurrency Error");

            session
                    .buildLockRequest(LockOptions.NONE)
                    .setLockMode(LockMode.OPTIMISTIC_FORCE_INCREMENT)
                    .setTimeOut(LockOptions.NO_WAIT);
            session.saveOrUpdate(info);

            session.flush();
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     * This is used for a list, if keywords blank, or a basic search by keyword
     **/
    public List<Information> getAllInfo(String keywords) {
        Session session = null;
        try {
            session = HibernateUtility.getSessionFactory().getSession();
            session.getTransaction().begin();

            //String sql = "select i.information_id, i.search_info, i.encrypted_info from main.java.hibernate.Information i";
            // I found I cannot select specific columns or it loses the column name and doesn't process it through the object method,
            // like decrypting the value.  So for now, select i will work.
            String sql = "select i from main.java.hibernate.Information i";
            String[] words = new String[0];
            if (!keywords.isBlank()) {
                sql += " where ";
                words = keywords.split(" ");
                for (String word : words) {
                    sql += "search_info like:k_" + word + " or ";
                }
                // Delete last 'or'
                sql = sql.substring(0, sql.length() - 4);
            }
            sql += " order by i.search_info";

            Query<Information> q = session.createQuery(sql);
            if (!keywords.isBlank()) {
                for (String word : words) {
                    q.setParameter("k_" + word, "%" + word + "%");
                }
            }
            List<Information> infoList = q.getResultList();


            session.getTransaction().commit();
            return infoList;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    public void deleteInfo(int id) {
        Session session = null;
        try {
            session = HibernateUtility.getSessionFactory().getSession();
            Transaction beginTransaction = session.beginTransaction();
            Query createQuery = session.createQuery("delete from main.java.hibernate.Information i where i.information_id =:id");
            createQuery.setParameter("id", id);
            createQuery.executeUpdate();
            beginTransaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}
