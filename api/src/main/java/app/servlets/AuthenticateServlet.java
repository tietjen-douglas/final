package main.java.app.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import main.java.Globals;
import main.java.dao.UserDAO;
import main.java.hibernate.User;
import main.java.utils.Encryption;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Duration;
import java.time.Instant;
import java.util.Enumeration;
import java.util.Map;

@WebServlet(name = "authenticate")
public class AuthenticateServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String jsonReturn = "{}";

        // Get any data information being posted in as a JSON value
        ObjectMapper mapper = new ObjectMapper();

        if (request.getParameter("j_username") != null && request.getParameter("j_password") != null) {
            String user = request.getParameter("j_username");
            String ePassword = request.getParameter("j_password");
            String dPassword = "";
            String pass = "";
            boolean auth_link_expired = false;

            // Decrypt String and check if valid
            Encryption encryption = new Encryption();
            try {
                dPassword = encryption.decryptString(Globals.EncryptionKey, ePassword);
                pass = dPassword.substring(dPassword.indexOf("_") + 1);

                //Compare time, give a 5 second window
                Instant instant = Instant.now();
                String passInstant = dPassword.substring(0, dPassword.indexOf("_"));
                System.out.println("passInstant: " + passInstant);

                Duration duration = Duration.between(Instant.parse(passInstant), instant);
                System.out.println("duration:" + duration);
                System.out.println("duration in seconds: " + duration.getSeconds());

                auth_link_expired = duration.getSeconds() > 4;

            } catch (Exception e) {
                e.printStackTrace();
            }

            System.out.println(dPassword);
            System.out.println(pass);

            if (!auth_link_expired) {
                UserDAO uDAO = new UserDAO();

                // Get account by Credentials
                User aUser = uDAO.getUserByCredentials(user, pass);
                if (aUser != null)
                    jsonReturn = mapper.writeValueAsString(aUser);
                else
                    setResponseMessage(response, 401, "Invalid Credentials");
            } else {
                setResponseMessage(response, 401, "Link has expired");
            }
        }
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.print(jsonReturn);
        out.flush();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.print("{get:Not available}");
        out.flush();
    }

    protected void setResponseMessage(HttpServletResponse response, int statusCode, String reason) {
        response.setStatus(statusCode);
        response.setHeader("reason", reason);
    }

}
