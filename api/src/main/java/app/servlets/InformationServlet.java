package main.java.app.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import main.java.dao.InfoDAO;
import main.java.hibernate.Information;
import main.java.utils.Encryption;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "InformationServlet")
public class InformationServlet extends HttpServlet {
    private InfoDAO iDAO = new InfoDAO();

    // Initialize Form Variables
    private int editID;
    boolean isNewInfo;
    Information editInfo;
    private String fSearchInfo;
    private String fEncryptedInfo;
    private int fVersion;
    private String errorMsg;

    protected void initializeVars() {
        fSearchInfo = "";
        fEncryptedInfo = "";
        fVersion = -1;
        errorMsg = "";
        editID = -1;
        isNewInfo = true;
        editInfo = null;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        initializeVars();

        String jsonReturn = "{}";

        // Adding or Updating Information
        if (request.getParameter("saveInfo") != null) {
            try {
                editID = Integer.parseInt(request.getParameter("saveInfo"));
            } catch (NumberFormatException e) {
                errorMsg += "<li>Invalid Information ID</li>";
            }
            isNewInfo = editID <= 0;
            fSearchInfo = request.getParameter("searchText");
            fEncryptedInfo = request.getParameter("encText");
            try {
                if (request.getParameter("version") != null)
                    fVersion = Integer.parseInt(request.getParameter("version"));
                else
                    fVersion = -1;
            } catch (Exception e) {
                errorMsg += "<li>Version invalid</li>";
            }
            // Validate the fields
            if (fSearchInfo == null || fSearchInfo.isBlank())
                errorMsg += "<li>Search text is required</li>";

            if (errorMsg.isEmpty()) {

                // Add or Update, no errors
                if (isNewInfo) {
                    Information newInfo = null;
                    //try {
                    try {
                        newInfo = new Information(fSearchInfo, fEncryptedInfo);
                    } catch (Encryption.CryptoException e) {
                        errorMsg += "<li>Error: " + e.getMessage() + "</li>";
                        e.printStackTrace();
                    }
                    try {
                        iDAO.addInfo(newInfo);
                    } catch (Exception e) {
                        e.printStackTrace();
                        recordError(e);
                    }
                } else {
                    editInfo = new Information();

                    editInfo.setInformation_id(editID);
                    editInfo.setSearch_info(fSearchInfo);
                    editInfo.setVersion(fVersion);
                    try {
                        editInfo.setEncrypted_info(fEncryptedInfo);
                    } catch (Encryption.CryptoException e) {
                        errorMsg += "<li>Error: " + e.getMessage() + "</li>";
                        e.printStackTrace();
                    }
                    try {
                        iDAO.updateInfo(editInfo);
                    } catch (Exception e) {
                        recordError(e);
                    }

                }
            }
        }

        sendReturnJSON(response, jsonReturn);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        initializeVars();

        String jsonReturn = "{}";
        // Get any data information being posted in as a JSON value
        ObjectMapper mapper = new ObjectMapper();

        try {
            if (request.getParameter("id") != null) {
                // Get a specific record of information
                Information inf = iDAO.getInfo(Integer.parseInt(request.getParameter("id")));
                jsonReturn = mapper.writeValueAsString(inf);
            } else if (request.getParameter("list") != null) {
                // Get list of information
                String keywords = request.getParameter("list");
                List<Information> inf = iDAO.getAllInfo(keywords);
                jsonReturn = mapper.writeValueAsString(inf);
            } else if (request.getParameter("del") != null) {
                // Delete User
                int delID = Integer.parseInt(request.getParameter("del"));
                iDAO.deleteInfo(delID);
            }
        } catch (Exception e) {
            recordError(e);
        }
        sendReturnJSON(response, jsonReturn);
    }

    private void recordError(Exception e) {
        // Unique usernames required
        if (e == null) {
            errorMsg += "<li>Unknown Error</li>";
        } else {
            errorMsg += "<li>Invalid: " + e.getMessage() + "</li>";
        }
    }

    protected void setResponseMessage(HttpServletResponse response, int statusCode, String reason) {
        response.setStatus(statusCode);
        response.setHeader("reason", reason);
    }

    protected void sendReturnJSON(HttpServletResponse response, String jsonReturn) throws IOException {
        // If error, throw error back.
        if (!errorMsg.isEmpty()) {
            setResponseMessage(response, 409, errorMsg);
        }

        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.print(jsonReturn);
        out.flush();
    }
}
