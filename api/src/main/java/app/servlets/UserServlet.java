package main.java.app.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import main.java.dao.UserDAO;
import main.java.hibernate.User;
import main.java.utils.Encryption;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@WebServlet(name = "UserUpdateServlet")
public class UserServlet extends HttpServlet {
    private UserDAO uDAO = new UserDAO();

    // Initialize Form Variables
    private int editID;
    boolean isNewUser;
    User editUser;
    private String fFullName;
    private String fUsername;
    private String fPassword;
    private String errorMsg;

    protected void initializeVars() {
        fFullName = "";
        fUsername = "";
        fPassword = "";
        errorMsg = "";
        editID = -1;
        isNewUser = true;
        editUser = null;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        initializeVars();

        String jsonReturn = "{}";

        // Get any data information being posted in as a JSON value
        ObjectMapper mapper = new ObjectMapper();

        // Adding or Updating a user
        if (request.getParameter("saveUser") != null) {

            try {
                editID = Integer.parseInt(request.getParameter("saveUser"));
            } catch (NumberFormatException e) {
                errorMsg += "<li>Invalid User ID</li>";
            }
            isNewUser = editID <= 0;
            fFullName = request.getParameter("fullName");
            fUsername = request.getParameter("username");
            fPassword = request.getParameter("password");

            // Validate the fields
            if (fFullName == null || fFullName.isBlank())
                errorMsg += "<li>Full Name is required</li>";
            if (fUsername == null || fUsername.isBlank())
                errorMsg += "<li>Username is required</li>";
            // If Password left blank on Edit Update then it won't modify the password
            if (isNewUser) {
                if (fPassword == null || fPassword.isBlank())
                    errorMsg += "<li>Password is required1</li>";
            } //else if (fPassword != null && fPassword.isBlank())
            //errorMsg += "<li>Password is required2</li>";

            if (errorMsg.isEmpty()) {

                // Add or Update, no errors
                if (isNewUser) {
                    User newUser = null;
                    try {
                        newUser = new User(fFullName, fUsername, fPassword);
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                        recordError(e);
                    }
                    try {
                        uDAO.addUser(newUser);
                    } catch (Exception e) {
                        e.printStackTrace();
                        recordError(e);
                    }
                } else {
                    editUser = uDAO.getUser(editID);
                    if (editUser == null)
                        errorMsg += "<li>Unable to find user.</li>";
                    else {
                        editUser.setFull_name(fFullName);
                        editUser.setUsername(fUsername);
                        if (fPassword != null && !fPassword.isBlank()) {
                            try {
                                editUser.setPassword(Encryption.hashPassword(editUser.getUser_id(), editUser.getSalt(), fPassword));
                            } catch (Exception e) {
                                recordError(e);
                            }
                        }
                        try {
                            uDAO.updateUser(editUser);
                        } catch (Exception e) {
                            recordError(e);
                        }
                    }
                }
            }
        } else if (request.getParameter("user") != null) {
            // Get a specific user
            try {
                User u = uDAO.getUser(Integer.parseInt(request.getParameter("user")));
                jsonReturn = mapper.writeValueAsString(u);
            } catch (Exception e) {
                recordError(e);
            }
        } else if (request.getParameter("userList") != null) {
            // Get list of users
            try {
                List<User> u = uDAO.getUsers();
                jsonReturn = mapper.writeValueAsString(u);
            } catch (Exception e) {
                recordError(e);
            }
        } else if (request.getParameter("deleteUser") != null) {
            // Delete User
            try {
                int delUserID = Integer.parseInt(request.getParameter("deleteUser"));
                uDAO.deleteUser(delUserID);
            } catch (Exception e) {
                recordError(e);
            }
        }


        if (!errorMsg.isEmpty()) {
            setResponseMessage(response, 409, errorMsg);
        }

        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.print(jsonReturn);
        out.flush();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.print("{Get:Not available}");
        out.flush();
    }

    private void recordError(Exception e) {
        // Unique usernames required
        if (e == null) {
            errorMsg += "<li>Unknown Error</li>";
        } else {
            if (e.getCause().toString().contains("username_UNIQUE") ||
                    e.getCause().toString().contains("ConstraintViolationException"))
                errorMsg += "<li>Username already taken, choose another.</li>";
            else
                errorMsg += "<li>" + e.getCause() + "</li>";
        }
    }

    protected void setResponseMessage(HttpServletResponse response, int statusCode, String reason) {
        response.setStatus(statusCode);
        response.setHeader("reason", reason);
    }
}
