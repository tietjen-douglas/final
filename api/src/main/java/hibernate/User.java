package main.java.hibernate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import main.java.utils.Encryption;

import javax.persistence.*;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

@Entity
@Table(name = "user")
public class User {
    /**
     * id is an identity type field in the database and the primary key
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int user_id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "full_name")
    private String full_name;

    @Column(name = "salt")
    private String salt;

    public User() throws UnsupportedEncodingException, NoSuchAlgorithmException {
    }

    public User(String full_name, String username) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String salt = Encryption.createSalt();
        this.salt = salt;
        this.full_name = full_name;
        this.username = username;
    }

    public User(String full_name, String username, String password) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        this(full_name, username);
        this.password = password;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    @JsonIgnore
    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String toString() {
        return Integer.toString(user_id) + ": " + full_name + " (" + username + ")";
    }
}
