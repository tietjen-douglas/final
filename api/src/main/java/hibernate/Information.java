package main.java.hibernate;

import main.java.Globals;
import main.java.utils.Encryption;

import javax.persistence.*;

import static javax.persistence.LockModeType.WRITE;

@Entity
@Table(name = "information")
@NamedQueries({
        @NamedQuery(name = Information.QUERY_BY_ID, query = "SELECT i FROM Information i WHERE i.information_id = :id"),
})

public class Information {
    public static final String QUERY_BY_ID = "query.Information.id";

    /**
     * id is an identity type field in the database and the primary key
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "information_id")
    private int information_id;

    @Column(name = "search_info")
    private String search_info;

    @Column(name = "encrypted_info")
    private String encrypted_info;

    @Version
    private Integer version;

    private static Encryption enc = new Encryption();

    public Information() {
    }

    public Information(String search_info, String encrypted_info) throws Encryption.CryptoException {
        this.search_info = search_info;
        this.encrypted_info = enc.encryptString(Globals.EncryptionKey, encrypted_info);
    }

    public int getInformation_id() {
        return information_id;
    }

    public void setInformation_id(int information_id) {
        this.information_id = information_id;
    }

    public String getSearch_info() {
        return search_info;
    }

    public void setSearch_info(String search_info) {
        this.search_info = search_info;
    }

    public String getEncrypted_info() throws Encryption.CryptoException {
        String dInfo = decryptData(encrypted_info);
        return dInfo;
    }

    public void setEncrypted_info(String encrypted_info) throws Encryption.CryptoException {
        String eInfo = encryptData(encrypted_info);
        this.encrypted_info = eInfo;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String toString() {
        return Integer.toString(information_id) + ": " + search_info;
    }

    private String encryptData(String encrypted_info) throws Encryption.CryptoException {
        String eData = enc.encryptString(Globals.EncryptionKey, encrypted_info);
        return eData;
    }

    private String decryptData(String encrypted_info) throws Encryption.CryptoException {
        String dData = enc.decryptString(Globals.EncryptionKey, encrypted_info);
        return dData;
    }
}
