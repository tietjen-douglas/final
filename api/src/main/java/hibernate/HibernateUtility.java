/* Original File written by Troy Tucket - BYU-Idaho CIT360
   Then modified by Douglas Tietjen from these references:
   https://docs.jboss.org/hibernate/orm/5.4/quickstart/html_single/
   https://www.onlinetutorialspoint.com/hibernate/hibernate-4-example-with-annotations.html
 */
package main.java.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtility {

    private static HibernateUtility me;
    private Configuration cfg;
    private SessionFactory sessionFactory;

    private HibernateUtility() throws HibernateException {

        cfg = new Configuration().configure();

        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure() // configures settings from hibernate.cfg.xml
                .build();

        try {
            sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception e) {
            // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
            // so destroy it manually.
            StandardServiceRegistryBuilder.destroy(registry);
        }
    }

    public static synchronized HibernateUtility getSessionFactory() throws HibernateException {
        // Use synchronized so that we will make sure we only get one instance.
        if (me == null) {
            me = new HibernateUtility();
        }

        return me;
    }

    public Session getSession() throws HibernateException {
        Session session = sessionFactory.openSession();
        if (!session.isConnected()) {
            this.reconnect();
        }
        return session;
    }

    private void reconnect() throws HibernateException {
        this.sessionFactory = cfg.buildSessionFactory();
    }

}